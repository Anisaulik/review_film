
@extends('master')

@section('content')
<div class="card">
  <div class="card-header">
    <h3 class="card-title">All Cast</h3>

    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
        <i class="fas fa-minus"></i>
      </button>
      <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
        <i class="fas fa-times"></i>
      </button>
    </div>
  </div>
  <div class="card-body">
    <div class="container">
      <h2>Bordered Table</h2>
      <a href="{{url('/cast/create')}}" class="btn btn-success">Tambah</a>
      <br>
      <br>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Umur</th>
            <th>Bio</th>
            <td>Aksi</td>
          </tr>
        </thead>
        <tbody>
          @forelse ($casts as $key=>$value)
          <tr>
              <td>{{$key + 1}}</th>
              <td>{{$value->nama}}</td>
              <td>{{$value->umur}}</td>
              <td>{{$value->bio}}</td>
              
              <td>
                  <a href="{{url('/cast')}}/{{$value->id}}" class="btn btn-info">Show</a>
                  <a href="{{url('/cast')}}/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                  <form action="{{url('/cast')}}/{{$value->id}}" method="POST">
                      @csrf
                      @method('DELETE')
                      <input type="submit" class="btn btn-danger my-1" value="Delete">
                  </form>
              </td>
          </tr>
      @empty
          <tr colspan="3">
              <td>No data</td>
          </tr>  
      @endforelse      
        </tbody>
      </table>
    </div>
    
  </div>
  <!-- /.card-body -->
  <div class="card-footer">
    Footer
  </div>
  <!-- /.card-footer-->
</div>
<!-- /.card -->   
@endsection
