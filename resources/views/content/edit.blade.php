
@extends('master')

@section('content')
<div class="card">
  <div class="card-header">
    <h3 class="card-title">Edit Cast {{$data->nama}} </h3>

    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
        <i class="fas fa-minus"></i>
      </button>
      <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
        <i class="fas fa-times"></i>
      </button>
    </div>
  </div>
  <div class="card-body">
    <div class="container">
      <form action="{{url('/cast')}}/{{$data->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
          <label for="email">Nama :</label>
          <input type="text" class="form-control" placeholder="Nama Cast" id="nama" name="nama" value="{{$data->nama}}">
          @error('nama')
          <div class="alert alert-danger">
              {{ $message }}
          </div>
      @enderror
        </div>
        <div class="form-group">
          <label for="pwd">Umur :</label>
          <input type="text" class="form-control" placeholder="Umur" id="umur" name="umur" value="{{$data->umur}}">
          @error('umur')
          <div class="alert alert-danger">
              {{ $message }}
          </div>
      @enderror
        </div>
        <div class="form-group">
          <label for="pwd">Bio :</label>
          <input type="text" class="form-control" placeholder="bio" id="bio" name="bio" value="{{$data->bio}}">
          @error('umur')
          <div class="alert alert-danger">
              {{ $message }}
          </div>
      @enderror
        
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>      
    </div>
    
  </div>
  <!-- /.card-body -->
  <div class="card-footer">
    Footer
  </div>
  <!-- /.card-footer-->
</div>
<!-- /.card -->   
@endsection



